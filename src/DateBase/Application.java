package DateBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Application {
	
	public static void main(String[] args) {
		
		/* DB접속을 위한 Connection 인스턴스 생성을 위한 레퍼런스 변수 선언 
		 * 나중에 finally 블럭에서 사용하기 위해 try블럭 밖에서 선언한다.*/
		Connection con = null;
		
		/* 사용할 드라이버 등록 
		 * OracleDriver.class를 불러서 사용하겠다. -> Class.forName(); */
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");	// 경로 차례차례 다써준다.
			
											//  데이터베이스에접속할수 있는 url
			con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe",
					"C##EMPLOYEE","EMPLOYEE");
			
			System.out.println("con : " + con);
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			if(con != null) {		
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
		}
		
		
		
		
		
		
	}
	
}
