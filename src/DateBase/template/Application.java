package DateBase.template;

import static DateBase.template.JDBCTem.close;
import static DateBase.template.JDBCTem.getConnection;

import java.sql.Connection;

public class Application {
	
	public static void main(String[] args) {
		
		Connection con = getConnection();
		System.out.println(con);
		
		
		close(con);
	}

}
